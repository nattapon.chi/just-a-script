# NiFi Template Variable Replacer

This groovy script can help to set or remove variable of apache nifi template via xml file

## Prerequisites

Groovy [Download Here](https://groovy.apache.org/download.html)

## Getting Started

Clone this project and execute script by using command line

#### Get usage information
```
groovy xml_replacer.groovy -h
```

#### Crate configuration files from template file
```
groovy xml_replacer.groovy -c <templatefilepath> <configfilepath>
```

#### Delete all nifi template variable
```
groovy xml_replacer.groovy -d <templatefilepath>
```

#### Replace all variable from configuration file to template file 
```
groovy xml_replacer.groovy -r <configfilepath> <templatefilepath>
```

#### Replace all variable from configutation file to new nifi template file
```
groovy xml_replacer.groovy -o <configfilepath> <templatefilepath> <outputfilepath>
```
