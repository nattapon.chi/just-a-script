@Grab(group='commons-cli', module='commons-cli', version='1.4')

import groovy.xml.*
import org.apache.commons.cli.Option

def cli = new CliBuilder()

cli.with{
    usage: 'groovy [options] <file>'
    h longOpt: 'help', 'Usage information'
    o longOpt: 'output', 'Create new output template (<configfile> <templatefile> <outputfile> (separate by space))', args: Option.UNLIMITED_VALUES, valueSeparator: ' ', argName: 'file'
    c longOpt: 'create', 'Crate configuration files (<templatefile> <configfile> (separate by space))', args: Option.UNLIMITED_VALUES, valueSeparator: ' ', argName: 'file'
    d longOpt: 'delete', 'Delete all nifi variable (templatefile)', args: 1, argName: 'file'
    r longOpt: 'replace', 'Replace all variable from configuration file to template file (<configfile> <templatefile>(separate by space))', args: Option.UNLIMITED_VALUES, valueSeparator: ' ', argName: 'file'
}
def options = cli.parse(args)

if( !options) {
    System.err << 'Error while parsing command-line options.\n'
    System.exit 1
}
if( options.h ) {
    cli.usage()
    return
}
if( options.c){
    def templateFile = options.cs[0]
    def configFile = options.cs[1]

    def template = new XmlSlurper().parse( new File(templateFile))

    def fileWriter = new FileWriter(configFile)
    def variableBuilder = new MarkupBuilder(fileWriter)
    variableBuilder.variable {
        template.snippet.processGroups.variables.entry.each{ variable ->
            entry {
                key(variable.key)
                value(variable.value)
            }
        }
    }
    fileWriter.close();
    println("Configuration file had created at " + configFile)
}


if( options.d ) {
    def templateFile = options.d
    template = new XmlSlurper().parse( new File(templateFile))
    template.snippet.processGroups.variables.entry.each{ variable ->
        variable.value = ""
    }
    def writer = new FileWriter(templateFile)
    XmlUtil.serialize(template, writer)
    println("Variable in "+ templateFile+ " had Deleted")
}
if(options.r){
    def configFile = options.rs[0]
    def templateFile = options.rs[1]
    def template = new XmlSlurper().parse( new File(templateFile))

    def variableConfigXml = new XmlSlurper().parse( new File(configFile))
    def count = 0
    template.snippet.processGroups.variables.entry.each{ variable ->
        variable.value = variableConfigXml.entry[count].value.text()
        count++
    }
    def writer = new FileWriter(templateFile)
    XmlUtil.serialize(template, writer)
    println("Replaced to " + templateFile)
}

if(options.o){
    def configFile = options.os[0]
    def templateFile = options.os[1]
    def outputFile = options.os[2]
    def template = new XmlSlurper().parse( new File(templateFile))

    def variableConfigXml = new XmlSlurper().parse( new File(configFile))
    def count = 0
    template.snippet.processGroups.variables.entry.each{ variable ->
        variable.value = variableConfigXml.entry[count].value.text()
        count++
    }
    def writer = new FileWriter(outputFile)
    XmlUtil.serialize(template, writer)
    println("Replaced to " + outputFile)
}
